package br.ccr.bean;

public class FiltraListaPiloto {
	
	private Integer idPiloto;
	private String nomePiloto;
	private String hora;
	private String numeroVolta;
	private String tempoVolta;
	private String velocidadeMedia;
	
	public FiltraListaPiloto() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdPiloto() {
		return idPiloto;
	}

	public void setIdPiloto(Integer idPiloto) {
		this.idPiloto = idPiloto;
	}

	public String getNomePiloto() {
		return nomePiloto;
	}

	public void setNomePiloto(String nomePiloto) {
		this.nomePiloto = nomePiloto;
	}

	public String getHora() {
		return hora;
	}

	public void setHora(String hora) {
		this.hora = hora;
	}

	public String getNumeroVolta() {
		return numeroVolta;
	}

	public void setNumeroVolta(String numeroVolta) {
		this.numeroVolta = numeroVolta;
	}

	public String getTempoVolta() {
		return tempoVolta;
	}

	public void setTempoVolta(String tempoVolta) {
		this.tempoVolta = tempoVolta;
	}

	public String getVelocidadeMedia() {
		return velocidadeMedia;
	}

	public void setVelocidadeMedia(String velocidadeMedia) {
		this.velocidadeMedia = velocidadeMedia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPiloto == null) ? 0 : idPiloto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FiltraListaPiloto other = (FiltraListaPiloto) obj;
		if (idPiloto == null) {
			if (other.idPiloto != null)
				return false;
		} else if (!idPiloto.equals(other.idPiloto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FiltraListaPiloto [idPiloto=" + idPiloto + ", nomePiloto=" + nomePiloto + ", hora=" + hora
				+ ", numeroVolta=" + numeroVolta + ", tempoVolta=" + tempoVolta + ", velocidadeMedia=" + velocidadeMedia
				+ "]";
	}

	
}
