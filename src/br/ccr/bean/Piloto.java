package br.ccr.bean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Piloto   {
	
	private Integer idPiloto;
	private String nomePiloto;
	private List<Long> hora = new ArrayList<Long>();
	private List<Integer> numeroVolta = new ArrayList<Integer>(); 
	private List<Long> tempoVolta = new ArrayList<Long>();
	private List<Double>  velocidadeMedia = new ArrayList<Double>();
	private Map<Integer, Long> mapVoltaTempo = new HashMap<Integer, Long>();
	
	public Piloto() {
		// TODO Auto-generated constructor stub
	}

	public Integer getIdPiloto() {
		return idPiloto;
	}

	public void setIdPiloto(Integer idPiloto) {
		this.idPiloto = idPiloto;
	}

	public String getNomePiloto() {
		return nomePiloto;
	}

	public void setNomePiloto(String nomePiloto) {
		this.nomePiloto = nomePiloto;
	}

	public List<Long> getHora() {
		return hora;
	}

	public void setHora(Long hora) {
		this.hora.add(hora);
	}

	public List<Integer> getNumeroVolta() {
		return numeroVolta;
	}

	public void setNumeroVolta(Integer numeroVolta) {
		this.numeroVolta.add(numeroVolta);
	}

	public List<Long> getTempoVolta() {
		return tempoVolta;
	}

	public void setTempoVolta(Long tempoVolta) {
		this.tempoVolta.add(tempoVolta);
	}

	public List<Double> getVelocidadeMedia() {
		return velocidadeMedia;
	}

	public void setVelocidadeMedia(Double velocidadeMedia) {
		this.velocidadeMedia.add(velocidadeMedia);
	}
	
	public Map<Integer, Long> getMapVoltaTempo() {

		return mapVoltaTempo;
	}

	public void setMapVoltaTempo(Integer volta , Long tempo) {
		
		this.mapVoltaTempo.put(volta, tempo);	
				
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPiloto == null) ? 0 : idPiloto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Piloto other = (Piloto) obj;
		if (idPiloto == null) {
			if (other.idPiloto != null)
				return false;
		} else if (!idPiloto.equals(other.idPiloto))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Piloto [idPiloto=" + idPiloto + ", nomePiloto=" + nomePiloto + ", hora=" + hora + ", numeroVolta="
				+ numeroVolta + ", tempoVolta=" + tempoVolta + ", velocidadeMedia=" + velocidadeMedia + "]";
	}


	

	




}
