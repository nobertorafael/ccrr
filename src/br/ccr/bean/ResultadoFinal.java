package br.ccr.bean;

public class ResultadoFinal {
	private String nome;
	private Long tempoChegada;
	private Long tempoAposVencedor;
	
	
	public Long getTempoChegada() {
		return tempoChegada;
	}
	public void setTempoChegada(Long tempoChegada) {
		this.tempoChegada = tempoChegada;
	}
	public Long getTempoAposVencedor() {
		return tempoAposVencedor;
	}
	public void setTempoAposVencedor(Long tempoAposVencedor) {
		this.tempoAposVencedor = tempoAposVencedor;
	}
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
