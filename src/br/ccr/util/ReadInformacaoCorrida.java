package br.ccr.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.ccr.bean.FiltraListaPiloto;

public class ReadInformacaoCorrida {
	
	/**
	 * Le o arquivo e separa por linha
	 * @return
	 * @throws IOException
	 */
	public List<FiltraListaPiloto> lerArquivo() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("arquivo_de_entrada"));
		List<String> linha = new ArrayList<String>();
		List<FiltraListaPiloto> listaFiltraListaPiloto = new ArrayList<FiltraListaPiloto>();
		FiltraListaPiloto filtraPiloto = null;
		while (br.ready()) {
			
			linha.add(br.readLine());	
		}
		br.close();

		for (int i = 1; i < linha.size(); i++) {
			filtraPiloto = new FiltraListaPiloto();
			String pegaLinha = linha.get(i);
			
			String[] splitLista = splitLista(pegaLinha);
			
			filtraPiloto.setHora(splitLista[0]);
			filtraPiloto.setIdPiloto(Integer.parseInt(splitLista[1]));
			filtraPiloto.setNomePiloto(splitLista[3]);
			filtraPiloto.setNumeroVolta(splitLista[4]);
			filtraPiloto.setTempoVolta(splitLista[5]);
			filtraPiloto.setVelocidadeMedia(splitLista[6]);
			listaFiltraListaPiloto.add(filtraPiloto);
		}
		
		return listaFiltraListaPiloto;
	}
	
	private String[] splitLista(String linha){
		
		Pattern p = Pattern.compile("\\s{2,}");
		Matcher m = p.matcher(linha);
		linha = m.replaceAll(" ");
		String[] linhas = linha.split(" ");

		return linhas;
	}
	
}
