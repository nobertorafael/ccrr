package br.ccr.util;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import br.ccr.bean.Piloto;
import br.ccr.bean.ResultadoFinal;

public class RetornaResultado {
	
	SeparaInformacaoPorPiloto separaInfomacao = new SeparaInformacaoPorPiloto();
	private List<Piloto> populaBean;
	private ResultadoFinal result=null;

	public void listaTodasInformacoes() throws IOException, ParseException {

		
		  melhorPosicaoCadaPiloto(); 
		  velocidadeMediaCadaPilto();
		  tempoFinal();
	}
	
	public void melhorPosicaoCadaPiloto() {
		System.out.println("============= MELHOR VOLTA DE CADA PILOTO =================");
		Map<Integer, Long> map = null;
		Long tempo;
		Integer numero;
		String nomePiloto = "";
		try {
			this.populaBean = separaInfomacao.populaBean();
			for (int i = 0; i < populaBean.size(); i++ ) {
				map = new HashMap<Integer, Long>();
				nomePiloto =  populaBean.get(i).getNomePiloto();
				for (int j = 0; j <  populaBean.get(i).getTempoVolta().size(); j++) {
					 tempo = populaBean.get(i).getTempoVolta().get(j);
					 numero = populaBean.get(i).getNumeroVolta().get(j);
					 map.put(numero, tempo);
				}
				melhorPosicao(nomePiloto,map);
			}
			
			System.out.println("+++++++++++++++++++++++++++++++++++++");
			
		} catch (IOException | ParseException e) {
			
			e.printStackTrace();
		}
		
	}
	private void melhorPosicao(String nomePiloto,Map<Integer, Long> map) {
		SimpleDateFormat formatter= new SimpleDateFormat("mm:ss.SSS");
		List<Entry<Integer, Long>> listaOrdena = new LinkedList<Map.Entry<Integer,Long>>(map.entrySet());
		Collections.sort(listaOrdena,new Comparator<Entry<Integer, Long>>() {

			@Override
			public int compare(Entry<Integer, Long> o1, Entry<Integer, Long> o2) {
				// TODO Auto-generated method stub
				return o1.getValue().compareTo(o2.getValue());
			}
		});
		System.out.println("===============" + nomePiloto + "=============");
		for (Entry<Integer, Long> entry : listaOrdena) {
			Date data = new Date(entry.getValue());
			
			System.out.println("Volta; "+entry.getKey() +" Tempo: " + formatter.format(data ));
		}
		
		
	}
	
	public void velocidadeMediaCadaPilto() {
		System.out.println("=========== VELOCIDADE M�DIA ===========");
		DecimalFormat df = new DecimalFormat("#,###.00");
		
		Double aux = new Double(0);
		Double soma = new Double(0);
		try {
			this.populaBean = separaInfomacao.populaBean();
			for (Piloto piloto : populaBean) {
				String nomePiloto = piloto.getNomePiloto();
				List<Double> velocidadeMedia = piloto.getVelocidadeMedia();
				for (int i = 0; i < velocidadeMedia.size(); i++) {
					aux += velocidadeMedia.get(i);
				}
				soma = aux/velocidadeMedia.size();
				System.out.println("===== PILOTO: " + nomePiloto +" == " + df.format(soma)+"s"  );
				aux = new Double(0);
			}
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	public void tempoFinal() {
		System.out.println("=================== VENCEDOR =======================");
		List<ResultadoFinal> rf = new ArrayList<ResultadoFinal>();
		
		try {
			populaBean = separaInfomacao.populaBean();
			for (int i = 0; i < populaBean.size(); i++) {
				result = new ResultadoFinal();
				result.setNome(populaBean.get(i).getNomePiloto());
				for (int j = 0; j < populaBean.get(i).getNumeroVolta().size(); j++) {
					result.setTempoChegada(populaBean.get(i).getHora().get(populaBean.get(i).getNumeroVolta().size()-1));
				}
				rf.add(result);
				
			}
			Collections.sort(rf,new Comparator<ResultadoFinal>() {

				@Override
				public int compare(ResultadoFinal o1, ResultadoFinal o2) {
					// TODO Auto-generated method stub
					return o1.getTempoChegada().compareTo(o2.getTempoChegada());
				}
			});
			int cont = 0;
			Long primeiro = 0L;
			for (ResultadoFinal resultadoFinal : rf) {
				if(cont == 0) {
					primeiro = resultadoFinal.getTempoChegada();
				}
				SimpleDateFormat forma = new SimpleDateFormat("HH:mm:ss.SSS");
				Date hora = new Date(resultadoFinal.getTempoChegada());
				resultadoFinal.setTempoAposVencedor(resultadoFinal.getTempoChegada());				
				System.out.println(cont+1 + "�LUGAR: " +resultadoFinal.getNome() + " " + forma.format(hora) +" -"+ tempoChegadaAposVencedor(resultadoFinal.getTempoChegada(),primeiro));
				cont++;
			}

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
	
	private String tempoChegadaAposVencedor(Long data, Long primLug) {
		SimpleDateFormat forma = new SimpleDateFormat("ss.SSS");
		Long aux = data - primLug;
		Date tempo = new Date(aux);
		
		return forma.format(tempo);
		
	}
	
}















