package br.ccr.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import br.ccr.bean.FiltraListaPiloto;
import br.ccr.bean.Piloto;

public class SeparaInformacaoPorPiloto {

	private ReadInformacaoCorrida ric = new ReadInformacaoCorrida();
	private Piloto piloto = null;
	private SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss.SSS");
	private SimpleDateFormat sdftm = new SimpleDateFormat("m:ss.SSS");
	private Map<Integer, Piloto> map = new LinkedHashMap<Integer, Piloto>();
	
	/**
	 * filtra as informações de cada piloto
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	private Map<Integer, Piloto> informacaoCadapiloto() throws IOException, ParseException {
		for (FiltraListaPiloto flp : ric.lerArquivo()) {

			piloto = new Piloto();
			Integer idPiloto = flp.getIdPiloto();
			Date parseObject = sdf.parse(flp.getHora().trim());
			Date tmpVolta = sdftm.parse(flp.getTempoVolta().trim());

			if (map.containsKey(idPiloto)) {

				map.get(idPiloto).setHora(parseObject.getTime());
				map.get(idPiloto).setNumeroVolta(Integer.parseInt(flp.getNumeroVolta()));
				map.get(idPiloto).setVelocidadeMedia(Double.parseDouble(flp.getVelocidadeMedia().replace(',', '.')));
				map.get(idPiloto).setTempoVolta(tmpVolta.getTime());
				map.get(idPiloto).setMapVoltaTempo(Integer.parseInt(flp.getNumeroVolta()), tmpVolta.getTime());

			} else {

				piloto.setIdPiloto(idPiloto);
				piloto.setNomePiloto(flp.getNomePiloto());
				piloto.setNumeroVolta(Integer.parseInt(flp.getNumeroVolta()));
				piloto.setHora(parseObject.getTime());
				piloto.setTempoVolta(tmpVolta.getTime());
				piloto.setVelocidadeMedia(Double.parseDouble(flp.getVelocidadeMedia().replace(',', '.')));
				piloto.setMapVoltaTempo(Integer.parseInt(flp.getNumeroVolta()), tmpVolta.getTime());
				map.put(idPiloto, piloto);

			}
		}

		return map;
	}
	/**
	 * Popula o objeto com as infirmações de cada piloto
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */

	public List<Piloto> populaBean() throws IOException, ParseException {
		List<Piloto> listPiloto = new ArrayList<Piloto>();
		Piloto pilotoBen = null;
		Map<Integer, Piloto> info = informacaoCadapiloto();

		for (Map.Entry<Integer, Piloto> mapLista : info.entrySet()) {
			pilotoBen = new Piloto();
			pilotoBen.setIdPiloto(mapLista.getKey());
			pilotoBen.setNomePiloto(mapLista.getValue().getNomePiloto());
			List<Long> hora = mapLista.getValue().getHora();
			for (Long filtraListaPiloto : hora) {
				pilotoBen.setHora(filtraListaPiloto);
			}
			List<Long> tempoVolta = mapLista.getValue().getTempoVolta();
			for (Long tempo : tempoVolta) {
				pilotoBen.setTempoVolta(tempo);
			}
			List<Integer> numeroVolta = mapLista.getValue().getNumeroVolta();
			for (Integer numVolta : numeroVolta) {
				pilotoBen.setNumeroVolta(numVolta);
			}
			List<Double> velMedia = mapLista.getValue().getVelocidadeMedia();
			for (Double vlMedia : velMedia) {
				pilotoBen.setVelocidadeMedia(vlMedia);
			}

			listPiloto.add(pilotoBen);
		}
		return listPiloto;

	}

}
